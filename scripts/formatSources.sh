#!/bin/bash

# Change into the script directory
SCRIPTDIR=$(readlink -f $(dirname "$0"))
pushd "${SCRIPTDIR}" > /dev/null
cd ..

function formatFolder {
    if [ -e .clang-format ] ; then
        # Without excluding directories
        codeFolder=$1
        format_files=$(find "${codeFolder}" -maxdepth 1 -type f -name "*.cpp")
        format_files+=" "
        format_files+=$(find "${codeFolder}" -maxdepth 1 -type f -name "*.h")

        # Exclude some directories
        # codeFolder=$1
        # exclude_codeFolder=./bin
        # format_files=`find "${codeFolder}" -type f -path "${exclude_codeFolder}" -prune`

        for file in $format_files
        do
            clang-format --style=file --verbose -i "$file"
        done
    else
        echo "There is no .clang-format in the project root"
        echo ""
    fi
}

formatFolder src
# formatFolder test

# Back to the original location
popd > /dev/null

exit 0
