#!/bin/bash

# Change into the script directory
SCRIPTDIR=$(readlink -f $(dirname "$0"))
pushd "${SCRIPTDIR}" > /dev/null

# Offer option -v : verbose
VERBOSE=0
if [ 1 -eq $# ] ; then
    if [ "-v" == "$1" ] ; then
        VERBOSE=1
    fi
fi

echo "::: Get test framework"
if [ ! -d "../test/googletest" ] ; then
    echo "Get google test framework"
    ./getGoogleTest.sh
fi

# Cmake followed by make...
pwd
cd ../test
mkdir -p ./bin
cd ./bin
cmake ..
if [ 1 -eq ${VERBOSE} ] ; then
    make VERBOSE=1
else
    make
fi
feedback=$?

if [ 0 -eq ${feedback} ] ; then
    echo "::: Run the Tests"
    pwd
    cd ..
    pwd
    pwd
    pwd
    pwd
    pwd
    pwd
    ./bin/Test_Sudoku --gtest_output=xml:./Test/tmp/gtestresults.xml
    feedback=$?
fi

# Back to the original location
popd > /dev/null

exit ${feedback}
