#include "testPtr.h"
#include <stdio.h>


namespace Ptr
{
class IParameter
{
  public:
    virtual void set(int val) = 0;
    virtual int get() = 0;
};

class Parameter : public IParameter
{
  public:
    void set(int val) override
    {
        val_ = val;
    }
    int get() override
    {
        return val_;
    }
    int val_;
};

class Parameter_Mock : public IParameter
{
  public:
    void set(int val) override
    {
        val_ = val;
    }
    int get() override
    {
        return val_;
    }
    int val_;
};

// template<unsigned int len>
class Tracer
{
  public:
    Tracer(IParameter** array)
        : array_(array)
    {
    }
    void print()
    {
        printf("idx 0: %d\n", array_[0]->get());
        printf("idx 1: %d\n", array_[1]->get());
    }

  private:
    Tracer()
    {
    }
    IParameter** array_;
};

void runTestPtr()
{
    Parameter a[5];
    IParameter* pa[5];
    Parameter_Mock b[5];
    IParameter* pb[5];


    for (int i = 0; i < 5; i++) {
        a[i].set(i * 2);
        b[i].set(i * 20);
        pa[i] = &a[i];
        pb[i] = &b[i];
    }

    {
        Tracer myTracer(pa);
        printf("%d\n", __LINE__);
        myTracer.print();
        printf("%d\n", __LINE__);
    }
    {
        Tracer myTracer(pb);
        myTracer.print();
    }
    printf("done\n");
    return 0;
}

} // namespace Ptr
