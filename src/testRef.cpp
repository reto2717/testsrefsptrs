#include "testRef.h"
#include <stdio.h>
#include <variant>

namespace Ref
{

class IParameter
{
  public:
    virtual void set(int val) = 0;
    virtual int get() = 0;
};

class Parameter : public IParameter
{
  public:
    void set(int val) override
    {
        val_ = val;
    }
    int get() override
    {
        return val_;
    }
    int val_;
};

class Parameter_Mock : public IParameter
{
  public:
    void set(int val) override
    {
        val_ = val;
    }
    int get() override
    {
        return val_;
    }
    int val_;
};

// template<unsigned int len>
class Tracer
{
  public:
    Tracer(Parameter (&array)[5])
        : array_(array)
    {
    }
    Tracer(Parameter_Mock (&array)[5])
        : array_(array)
    {
    }
    void print()
    {
        printf("idx 0: %d\n", array_[0].get());
        printf("idx 1: %d\n", array_[1].get());
    }

  private:
    Tracer() = delete;
    std::variant<Parameter, Parameter_Mock> (&array_)[5];
};

void runTestRef()
{
    Parameter a[5];
    Parameter_Mock b[5];

    a[0].set(99);
    a[1].set(11);

    {
        Tracer myTracer(a);
        printf("%d\n", __LINE__);
        myTracer.print();
        printf("%d\n", __LINE__);
    }
    {
        Tracer myTracer(b);
        myTracer.print();
    }
    printf("done\n");
}

} // namespace Ref
