#include "testPtr.h"
#include "testRef.h"

#if 0
// Explanation why an array of references is not possible

// Pointer declartion would be inconsisten
int original = 5;
int& alias = original; // refers to 5
int* p_original = &original; // let's assume that: p_original == 0x01231234
int* p_alias = &alias; // p_alias == 0x01231234
(int&)* pr_alias = &alias; // error! (&alias has the type int*)

// Reference MUST be initializied. Arrays in C/C++ are not. The following would become mandatory:
int a,b,c;
int& v[] = { a, b, c };
// v[0] aliases a, v[1] does b and v[2] does c.
#endif

int main()
{
    Ref::runTestRef();
    Ptr::runTestPtr();

    return 0;
}
